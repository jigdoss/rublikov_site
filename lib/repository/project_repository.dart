import '../model/model.dart';

class ProjectRepository {
  final projectList = [
    Project(
      title: 'Pixi Walls - HD Wallpapers',
      appLogo: 'https://play-lh.googleusercontent.com/0AABKLfoNQACiNKryZQwuX9f-7zDJatbg0jMuhn4njnT8FhOMhvY_n4t0V1l2Se0IW0=w240-h480-rw',
      description:
      "Best Android App for Stunning Backgrounds: HD Wallpapers 🇮🇳 \nFor your Android device, are you looking for a stunning collection of Full HD wallpapers? Full HD Wallpaper is the best app for beautiful backgrounds, so look no further!",
      website: '',
      gitHub: '',
      demo: '',
      screenshot: [],
      miStore: '',
      playStore: 'https://play.google.com/store/apps/details?id=com.adbytee.pixi_walls',
      projectType: 'Android App',
      downloads: '50+',
      tags: ['Flutter', 'Dart', 'Android', 'iOS', 'Mobile', 'Wallpaper', 'HD', '4K', 'Pixi Walls'],
    ),
  ];
}