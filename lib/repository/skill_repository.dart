import 'package:rublikov_site/model/model.dart';

class SkillRepository {
  final skills = [
    Skill(title: 'Api', icons: 'assets/icons/api.png'),
    Skill(title: 'Css', icons: 'assets/icons/css.png'),
    Skill(title: 'Dart', icons: 'assets/icons/dart.png'),
    Skill(title: 'Database', icons: 'assets/icons/database.png'),
    Skill(title: 'Figma', icons: 'assets/icons/figma.png'),
    Skill(title: 'Flutter', icons: 'assets/icons/flutter.png'),
    Skill(title: 'GitHub', icons: 'assets/icons/github.png'),
    Skill(title: 'Html', icons: 'assets/icons/html.png'),
  ];
}