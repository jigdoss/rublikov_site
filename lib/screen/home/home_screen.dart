import 'package:flutter/material.dart';
import 'package:rublikov_site/colors.dart';
import 'package:rublikov_site/responsive.dart';
import 'package:rublikov_site/screen/home/widgets/footer.dart';
import 'package:rublikov_site/screen/home/widgets/hero.dart';
import 'package:rublikov_site/widgets/widgets.dart';

import 'widgets/project.dart';
import 'widgets/skills.dart';

class HomeScreen extends StatelessWidget {
  final GlobalKey<ScaffoldState> drawerKey = GlobalKey<ScaffoldState>();

  final homeKey = GlobalKey();
  final aboutKey = GlobalKey();
  final projectKey = GlobalKey();
  final skillsKey = GlobalKey();
  final topKey = GlobalKey();

  HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      key: drawerKey,
      backgroundColor: kWhite,
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            physics: const BouncingScrollPhysics(),
            child: SizedBox(
              width: Res.isMobile(context) ? size.width : size.width * 0.8,
              child: Column(
                children: [
                  const HeroWidget(),
                  const CustomDivider(),
                  Project(key: projectKey),
                  const CustomDivider(),
                  Skills(key: skillsKey),
                  const CustomDivider(),
                  Footer(
                    clickToTop: () {
                      if (Res.isDesktop(context)) {
                        Scrollable.ensureVisible(
                          homeKey.currentState!.context,
                          duration: const Duration(seconds: 2),
                        );
                      } else {
                        Scrollable.ensureVisible(
                          topKey.currentState!.context,
                          duration: const Duration(seconds: 2),
                        );
                      }
                    },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
