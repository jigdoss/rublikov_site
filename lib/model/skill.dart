class Skill {
  final String? title, icons;

  Skill({required this.title, required this.icons});
}